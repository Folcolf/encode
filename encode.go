package main

import (
	"bufio"
	"fmt"
	"github.com/alexflint/go-arg"
	"os"
	"strings"
)

var key []byte
var idxKey int
var encoded bool
var proofEncoded string

// init create variables
func init() {
	idxKey = 0
	key = []byte{
		0x7a, 0x23, 0x4a, 0x1d,
		0xba, 0xf8, 0xa0, 0x8e,
		0x6a, 0x8d, 0xb5, 0xb8,
		0x40, 0x27, 0xcb, 0x54}
	proofEncoded = "encoded"
	arg.MustParse(&args)
}

// encodeFile create a file encoded
func encodeFile(inputFile string, outputFile string) {
	file, err := os.Open(inputFile)
	fileWrite, errFw := os.Create(outputFile)
	if err != nil {
		panic(err)
	}
	if errFw != nil {
		panic(errFw)
	}
	defer func() {
		if err = file.Close(); err != nil {
			panic(err)
		}
		if errFw = fileWrite.Close(); errFw != nil {
			panic(errFw)
		}
	}()
	readWrite(file, fileWrite)
	fmt.Println("File " + outputFile + " created")
}

// readWrite func who read and write 256 bytes
func readWrite(file *os.File, fileWrite *os.File) {
	reader := bufio.NewReader(file)
	writer := bufio.NewWriter(fileWrite)
	bytes := make([]byte, 256)
	cpt := 0
	lengthProof := len(proofEncoded)
	var encodedText []byte
	for {
		nmb, err := reader.Read(bytes)
		if nmb == 0 {
			break
		}
		if err != nil {
			panic(err)
		}
		if cpt == 0 {
			if encoded = isEncoded(bytes[:lengthProof]); encoded {
				encodedText = encode(key, bytes[lengthProof:nmb])
			} else {
				_, errW := writer.Write(encode(key, []byte(proofEncoded)))
				idxKey = 0
				if errW != nil {
					panic(errW)
				}
				encodedText = encode(key, bytes[:nmb])
			}
		} else {
			encodedText = encode(key, bytes[:nmb])
		}
		_, errW := writer.Write(encodedText)
		if errW != nil {
			panic(errW)
		}
		cpt++
	}
	writer.Flush()
}

// encode Xor bytes to encode
func encode(key, text []byte) []byte {
	lenText := len(text)
	lenKey := len(key)
	for i := 0; i < lenText; i++ {
		if idxKey == lenKey {
			idxKey = 0
		}
		text[i] = text[i] ^ key[idxKey]
		idxKey++
	}
	return text
}

// isEncoded verify if proof encoded exist
func isEncoded(text []byte) bool {
	proofDecoded := encode(key, text)
	idxKey = 0
	res := strings.EqualFold(string(proofDecoded), proofEncoded)
	encode(key, text)
	idxKey = 0
	return res
}
