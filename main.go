package main

var args struct {
	File   string `arg:"positional, required" help:"file to encode"`
	Result string `arg:"positional, required" help:"file encoded"`
}

func main() {
	encodeFile(args.File, args.Result)
}
